package localchat.server;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.ServerSocket;

public class Server implements Runnable {
	
	private ServerSocket sock;
	private int port;
	private boolean isRunning = false; // just in case, and because while (true) is technically bad practice.
	
	private Thread thrRun, thrManageCli, thrSend, thrReceive; // MULTI-THREAD DRIFTING (hope you've got a threadripper)
	
	public Server(int port) {
		this.port = port;
		try {
			sock = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		thrRun = new Thread(this, "serbr");
		thrRun.start();
	}
	
	public void run() {
		isRunning = true;
		System.out.println("Server started on port " + port);
		manageClients();
		receivePack();
	}
	
	// check to see clients are still there or have disconnected abruptly
	private void manageClients() {
		thrManageCli = new Thread("Manager") {
			public void run() {
				while (isRunning) {
					// Management time
				}
			}
		};
		thrManageCli.start();
	}
	
	// CONSOOOM
	private void receivePack() {
		thrReceive = new Thread("Receiver") {
			public void run() {
				String messageIn;
				while (isRunning) {
					try {
						Socket connectSock = sock.accept();
						BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectSock.getInputStream()));
						DataOutputStream outToClient = new DataOutputStream(connectSock.getOutputStream());
						
						messageIn = inFromClient.readLine();
						System.out.println("Received the message: " + messageIn);
						outToClient.writeBytes(messageIn);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		};
		thrReceive.start();
	}
}
