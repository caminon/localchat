package localchat.server;

// hoping to get this to start with the first client in the future but for now it'll be its own application.
public class ServerMain {
	
	private Server serv;
	
	public ServerMain(int port) {
		serv = new Server(port);
	}
	
	public static void main(String[] args) {
		new ServerMain(37747);
	}
}
