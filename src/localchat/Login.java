package localchat;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;


/**
 * It's everyone's favorite starter singleton!
 * I just thought it would be easy to put main in here. Gonna see how long that lasts.
 * @author Caminon
 *
 */
public class Login extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private JPanel interactables;
	private JTextField nameField;
	
	private final int windowWidth = 300;
	private final int windowHeight = 420;
	
	private final int port = 37747; // gonna keep this constant for testing purposes
	// maybe I should put this into a config.java or something
	
	/**
	 * God I suck at UI programming.
	 * Did learn about anonymous objects though. Didn't know something like arrow functions in JS existed in Java too, but I suppose it's not much of a surprise.
	 * Do other OOP languages have anonymous object declarations like this too?
	 */
	public Login() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		setResizable(false);
		setTitle("LocalChat Login");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		setSize(windowWidth, windowHeight);
		setLocationRelativeTo(null);
		
		interactables = new JPanel();
		interactables.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(interactables);
		interactables.setLayout(null);
		
		JLabel greetingLabel = new JLabel("Welcome to LocalChat!");
		greetingLabel.setBounds(windowWidth / 2 - 66, windowHeight / 6, 164, 28);
		interactables.add(greetingLabel);
		
		JLabel nameLabel = new JLabel("Enter your name:");
		nameLabel.setBounds(windowWidth / 2 - 48, windowHeight / 2 - 75, 116, 28);
		interactables.add(nameLabel);
		
		nameField = new JTextField();
		
		nameField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent enterKey) {
				login(nameField.getText());
			}
		});
		
		nameField.setBounds(windowWidth / 2 - 82, windowHeight / 2 - 45, 164, 28);
		interactables.add(nameField);
		nameField.setColumns(10);
		
		JButton loginButton = new JButton("Login");
		loginButton.setBounds(windowWidth / 2 - 47, windowHeight * 2 / 3, 100, 28);
		
		loginButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent clicked) {
				login(nameField.getText());
			}
		});
		
		interactables.add(loginButton);
	}
	
	/**
	 * Prompts the program to spawn the client window and try to connect to the instance.
	 * LocalChat instances will have a port dedicated to them (something probably unused like 34873)
	 * If the client can't find a server, it will create one on the computer it is running on.
	 * By design, I want localchat to be P2P so that if the original host leaves it doesn't just destroy the chatroom.
	 * Say, how do you get javadoc to acknowledge a return character?
	 */
	private void login(String name) {
		dispose(); // bye-bye login window
		new Client(name, port);
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
