package localchat;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
// holy mother of imports
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 * BLOAT - probably gonna refactor this guy so networking code is in its own class and window code is in another.
 * @author Caminon
 *
 */
public class Client extends JFrame {
	public static final long serialVersionUID = 1L;
	
	private JPanel theStuff;
	private JTextField messageBox;
	private JTextArea chatView;
	
	private final int windowWidth = 1280;
	private final int windowHeight = 720;
	
	private String name;
	
	private Socket sock; // of course we using TCP from the wacky java.net package
	private InetAddress ip; // ip address has to be stored this way cuz muh object oriented
	
	private Thread thrSend; // mm yeah ooh yeah async time
	
	public Client(String name, int port) {
		this.name = name;
		boolean isConnected = openConnection(port);
		
		if (!isConnected) { // try catch isn't real what are you talking about
			System.err.println("Connection Failed!");
			logToChat("Connection Failed!");
		}
		
		createWindow();
		setTitle("LocalChat - " + name + " - Running on port " + port);
		
		logToChat("Attempting to connect as " + name + " on port " + port);
		sendMessage(name + " connected on port " + port);
	}
	
	private boolean openConnection(int port) {
		try { // except now it does
			ip = InetAddress.getByName("localhost"); // I could modify this to connect over the internet, but I think I'll save that for another time.
			sock = new Socket(ip, port);
		} catch (UnknownHostException e) {
			System.err.println("Zoinks scoob, ip failed to understand its target!");
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			System.err.println("Zoinks scoob, the socket couldn't find the send/receive stuff!");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	// I never got far enough to use this function...
	// Ecoutez...
	private String receivePack() throws IOException {
		// open docks for boats to arrive
		InputStream heyListen = sock.getInputStream();
		DataInputStream dataStream = new DataInputStream(heyListen);
		int longth = dataStream.readInt();
		byte[] messageRAW = new byte[longth];
		if (longth > 0) {
			dataStream.readFully(messageRAW);
		}
		return new String(messageRAW); // don't know if this is how you do it...
	}
	
	// ...et parlez
	public void sendPack(byte[] messageRAW) {
		thrSend = new Thread("Sender Thread") {
			public void run() {
				// put boat into stream
				try {
					OutputStream outStream = sock.getOutputStream();
					DataOutputStream dataStream = new DataOutputStream(outStream);
					
					dataStream.writeInt(messageRAW.length);
					if (messageRAW.length > 0) {
						dataStream.write(messageRAW);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		thrSend.start();
	}
	
	// cleaning up the constructor
	private void createWindow() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		setSize(windowWidth, windowHeight);
		setLocationRelativeTo(null);
		
		theStuff = new JPanel();
		theStuff.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(theStuff);
		
		GridBagLayout layoutTheStuff = new GridBagLayout();
		layoutTheStuff.columnWidths = new int[] {windowWidth * 1 / 10, windowWidth * 4 / 5, windowWidth *  1 / 6, windowWidth * 1 / 15}; // Sum of all elements in this array MUST be equal to windowWidth!
		layoutTheStuff.rowHeights = new int[] {10, windowHeight * 4 / 5, 50, 10}; // Sum of all elements in this array MUST be equal to windowHeight!
		layoutTheStuff.columnWeights = new double[] {1.0, Double.MIN_VALUE};
		layoutTheStuff.rowWeights = new double[] {1.0, Double.MIN_VALUE};
		theStuff.setLayout(layoutTheStuff);
		
		chatView = new JTextArea();
		chatView.setEditable(false);
		JScrollPane epicScroller = new JScrollPane(chatView);
		GridBagConstraints scrollerConstraints = new GridBagConstraints();
		scrollerConstraints.insets = new Insets(0, 0, 5, 5);
		scrollerConstraints.fill = GridBagConstraints.BOTH;
		scrollerConstraints.gridx = 1;
		scrollerConstraints.gridy = 1;
		scrollerConstraints.gridwidth = 2;
		theStuff.add(epicScroller, scrollerConstraints);
		
		messageBox = new JTextField();
		
		// Conveniently enough, JTextFields have an action listener for the enter key baked right into them.
		messageBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent enterKey) {
				if (!messageBox.getText().equals("")) {
					logToChat("<" + name + "> " + messageBox.getText());
					messageBox.setText("");
				}
			}
		});
		
		GridBagConstraints messageConstraints = new GridBagConstraints();
		messageConstraints.insets = new Insets(0, 0, 0, 5);
		messageConstraints.fill = GridBagConstraints.HORIZONTAL;
		messageConstraints.gridx = 1;
		messageConstraints.gridy = 2;
		theStuff.add(messageBox, messageConstraints);
		messageBox.setColumns(10);
		
		JButton sendMessageBtn = new JButton("Send");
		
		sendMessageBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent click) {
				sendMessage(messageBox.getText());
			}
		});
		
		GridBagConstraints buttonConstraints = new GridBagConstraints();
		buttonConstraints.insets = new Insets(0, 0, 0, 5);
		buttonConstraints.gridx = 2;
		buttonConstraints.gridy = 2;
		theStuff.add(sendMessageBtn, buttonConstraints);
		
		setVisible(true);
		
		messageBox.requestFocusInWindow();
	}
	
	private void sendMessage(String message) {
		if (!messageBox.getText().equals("")) {
			logToChat("<" + name + "> " + message);
			sendPack(message.getBytes());
			messageBox.setText("");
		}
	}
	
	public void logToChat(String log) {
		chatView.append(log + "\r\n");
	}
}
